# My Gitlab CI templates

Projet contenant les templates Gitlab CI de formation.

- App Server : [gitlab-ci-maven.yml](gitlab-ci-maven.yml)
- App Client : [gitlab-ci-angular.yml](gitlab-ci-maven.yml)
- Ansible Gitlab flow : [gitlab-ci-ansible-gitlab-flow.yml](gitlab-ci-maven.yml)